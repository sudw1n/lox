use miette::Result;
use std::cmp::Ordering;
use std::env;

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    match args.len().cmp(&2) {
        Ordering::Greater => {
            eprintln!("Usage: {} [script]", &args[0]);
        }
        Ordering::Equal => {
            lox::run_file(&args[1])?;
        }
        Ordering::Less => {
            lox::run_prompt()?;
        }
    }
    Ok(())
}
