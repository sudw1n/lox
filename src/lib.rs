pub mod error;
pub mod interpreter;
pub mod parser;
pub mod resolver;
pub mod scanner;
pub use std::{
    fs::File,
    io::{self, BufRead, BufReader, Read, Write},
};

pub use error::LoxError;
use interpreter::{Interpreter, RuntimeReturnValue};
use miette::NamedSource;
use parser::Parser;
use resolver::Resolver;
use scanner::Scanner;

pub fn run(source: String, interpreter: &mut Interpreter) -> miette::Result<()> {
    let mut scanner = Scanner::new(source);
    let tokens = scanner.scan_tokens()?;
    let mut parser = Parser::new(tokens);
    let statements = parser.parse()?;
    let mut resolver = Resolver::new(interpreter);
    if let Err(RuntimeReturnValue::Error(e)) = resolver.resolve_stmts(&statements) {
        return Err(e.into());
    }
    interpreter.interpret(&statements)?;
    Ok(())
}

pub fn run_file(file_path: &str) -> miette::Result<()> {
    let mut f = File::open(file_path).map_err(LoxError::IOError)?;
    let mut input = String::new();
    let mut interpreter = Interpreter::new();
    f.read_to_string(&mut input).map_err(LoxError::IOError)?;
    // TODO: need to find a way that doesn't require cloning the input while also
    // being able to report snippets using `miette`
    run(input.clone(), &mut interpreter).map_err(|err| {
        let named_source = NamedSource::new(file_path, input);
        err.with_source_code(named_source)
    })?;
    Ok(())
}

pub fn run_prompt() -> miette::Result<()> {
    let mut input = BufReader::new(io::stdin());
    let mut interpreter = Interpreter::new();
    loop {
        print!("> ");
        let mut line = String::new();
        io::stdout().flush().map_err(LoxError::IOError)?;
        input.read_line(&mut line).map_err(LoxError::IOError)?;
        if line.is_empty() {
            break;
        }
        run(line.clone(), &mut interpreter).map_err(|err| err.with_source_code(line))?;
    }
    Ok(())
}
