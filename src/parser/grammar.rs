//! Our current grammar:
//! program -> declaration* EOF ;
//! declaration -> class_decl | fun_decl | var_decl | statement ;
//! class_decl -> "class" IDENTIFIER ( "<" IDENTIFIER )? "{" function* "}" ;
//! fun_decl -> "fun" function ;
//! function -> IDENTIFIER "(" parameters? ")" block ;
//! parameters -> IDENTIFIER ( "," IDENTIFIER )* ;
//! var_decl -> "var" IDENTIFIER ( "=" expression )? ";" ;
//! statement -> exprstmt
//!             | forstmt
//!             | ifstmt
//!             | printstmt
//!             | whilestmt
//!             | returnstmt
//!             | block ;
//! returnstmt -> "return" expression? ";" ;
//! forstmt -> "for" "(" ( var_decl | exprstmt | ";" )
//!             expression? ";"
//!             expression? ")" statement ;
//! whilestmt -> "while" "(" expression ")" statement ;
//! ifstmt -> "if" "(" expression ")" statement ( "else" statement )? ;
//! block -> "{" declaration* "}" ;
//! exprstmt -> expression ";" ;
//! printstmt -> "print" expression ";" ;
//! expression -> assignment ;
//! assignment -> (call "." )? IDENTIFIER "=" assignment | logic_or ;
//! logic_or -> logic_and ( "or" logic_and )* ;
//! logic_and -> equality ( "and" equality )* ;
//! equality -> comparison ( ( "!=" | "==" ) comparison )* ;
//! comparison -> term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
//! term -> factor ( ("+" | "-") factor )* ;
//! factor -> unary ( ( "/" | "*" ) unary )* ;
//! unary -> ( "!" | "-" ) unary | call ;
//! call -> primary ( "(" arguments? ")" | "." IDENTIFIER )* ;
//! arguments -> expression ( "," expression )* ;
//! primary -> "true" | "false" | "nil"
//!           | "(" expression ")"
//!           | "super" "." IDENTIFIER
//!           | IDENTIFIER ;

use crate::scanner::token::Token;
use std::fmt;

/// This macro generates classes for the various expression and statement types.
macro_rules! generate_type {
    ($class:ident : $($field:ident : $type:ty),*) => {
        #[derive(Debug, Clone, PartialEq)]
        pub struct $class {
            $(pub $field: $type),*
        }

        impl $class {
            // it's okay if we don't use the new() function,
            // it's just there for convenience
            #[allow(dead_code)]
            pub fn new($($field: $type),*) -> Self {
                $class {
                    $($field),*
                }
            }
        }
    };
}

// expression types
generate_type!(BinaryExpr: left: Box<Expr>, operator: Token, right: Box<Expr>);
generate_type!(GroupingExpr: expression: Box<Expr>);
generate_type!(UnaryExpr: operator: Token, right: Box<Expr>);
generate_type!(VarExpr: name: Token);
generate_type!(AssignExpr: name: Token, value: Box<Expr>);
// couldn't do it with generate_type!, so just went manually
#[derive(Debug, Clone, PartialEq)]
pub enum LiteralExpr {
    Boolean(bool),
    Nil,
    Number(f64),
    Str(String),
}
generate_type!(LogicalExpr: left: Box<Expr>, operator: Token, right: Box<Expr>);
generate_type!(CallExpr: callee: Box<Expr>, closing_paren: Token, arguments: Vec<Expr>);
generate_type!(GetExpr: object: Box<Expr>, name: Token);
generate_type!(SetExpr: object: Box<Expr>, name: Token, value: Box<Expr>);
generate_type!(ThisExpr: keyword: Token);
generate_type!(SuperExpr: keyword: Token, method: Token);

// statement types
generate_type!(VarStmt: name: Token, initializer: Option<Expr>);
generate_type!(BlockStmt: statements: Vec<Stmt>);
generate_type!(IfStmt: condition: Expr, then_branch: Box<Stmt>, else_branch: Option<Box<Stmt>>);
generate_type!(WhileStmt: condition: Expr, body: Box<Stmt>);
generate_type!(FunctionStmt: name: Token, params: Vec<Token>, body: Vec<Stmt>);
generate_type!(ReturnStmt: keyword: Token, value: Expr);
generate_type!(ClassStmt: name: Token, superclass: Option<Expr>, methods: Vec<Stmt>);

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    Binary(BinaryExpr),
    Grouping(GroupingExpr),
    Unary(UnaryExpr),
    Literal(LiteralExpr),
    Variable(VarExpr),
    Assign(AssignExpr),
    Logical(LogicalExpr),
    Call(CallExpr),
    Get(GetExpr),
    Set(SetExpr),
    This(ThisExpr),
    Super(SuperExpr),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Stmt {
    Expression(Expr),
    Print(Expr),
    Variable(VarStmt),
    Block(BlockStmt),
    If(IfStmt),
    While(WhileStmt),
    Function(FunctionStmt),
    Return(ReturnStmt),
    Class(ClassStmt),
}

pub trait ExprVisitor<R, E> {
    fn visit_binary_expr(&mut self, expr: &BinaryExpr) -> Result<R, E>;
    fn visit_grouping_expr(&mut self, expr: &GroupingExpr) -> Result<R, E>;
    fn visit_unary_expr(&mut self, expr: &UnaryExpr) -> Result<R, E>;
    fn visit_literal_expr(&mut self, value: &LiteralExpr) -> Result<R, E>;
    fn visit_var_expr(&mut self, value: &VarExpr) -> Result<R, E>;
    fn visit_assign_expr(&mut self, value: &AssignExpr) -> Result<R, E>;
    fn visit_logical_expr(&mut self, expr: &LogicalExpr) -> Result<R, E>;
    fn visit_call_expr(&mut self, expr: &CallExpr) -> Result<R, E>;
    fn visit_get_expr(&mut self, expr: &GetExpr) -> Result<R, E>;
    fn visit_set_expr(&mut self, expr: &SetExpr) -> Result<R, E>;
    fn visit_this_expr(&mut self, expr: &ThisExpr) -> Result<R, E>;
    fn visit_super_expr(&mut self, expr: &SuperExpr) -> Result<R, E>;
}

impl Expr {
    pub fn accept<R, E>(&self, visitor: &mut dyn ExprVisitor<R, E>) -> Result<R, E> {
        match self {
            Expr::Binary(expr) => visitor.visit_binary_expr(expr),
            Expr::Grouping(expr) => visitor.visit_grouping_expr(expr),
            Expr::Unary(expr) => visitor.visit_unary_expr(expr),
            Expr::Literal(expr) => visitor.visit_literal_expr(expr),
            Expr::Assign(expr) => visitor.visit_assign_expr(expr),
            Expr::Variable(expr) => visitor.visit_var_expr(expr),
            Expr::Logical(expr) => visitor.visit_logical_expr(expr),
            Expr::Call(expr) => visitor.visit_call_expr(expr),
            Expr::Get(expr) => visitor.visit_get_expr(expr),
            Expr::Set(expr) => visitor.visit_set_expr(expr),
            Expr::This(expr) => visitor.visit_this_expr(expr),
            Expr::Super(expr) => visitor.visit_super_expr(expr),
        }
    }
}

pub trait StmtVisitor<R, E> {
    fn visit_expression_stmt(&mut self, expr: &Expr) -> Result<R, E>;
    fn visit_print_stmt(&mut self, expr: &Expr) -> Result<R, E>;
    fn visit_var_stmt(&mut self, stmt: &VarStmt) -> Result<R, E>;
    fn visit_block_stmt(&mut self, stmt: &BlockStmt) -> Result<R, E>;
    fn visit_if_stmt(&mut self, stmt: &IfStmt) -> Result<R, E>;
    fn visit_while_stmt(&mut self, stmt: &WhileStmt) -> Result<R, E>;
    fn visit_function_stmt(&mut self, stmt: &FunctionStmt) -> Result<R, E>;
    fn visit_return_stmt(&mut self, stmt: &ReturnStmt) -> Result<R, E>;
    fn visit_class_stmt(&mut self, stmt: &ClassStmt) -> Result<R, E>;
}

impl Stmt {
    pub fn accept<R, E>(&self, visitor: &mut dyn StmtVisitor<R, E>) -> Result<R, E> {
        match self {
            Stmt::Print(expr) => visitor.visit_print_stmt(expr),
            Stmt::Expression(expr) => visitor.visit_expression_stmt(expr),
            Stmt::Variable(varstmt) => visitor.visit_var_stmt(varstmt),
            Stmt::Block(blockstmt) => visitor.visit_block_stmt(blockstmt),
            Stmt::If(ifstmt) => visitor.visit_if_stmt(ifstmt),
            Stmt::While(whilestmt) => visitor.visit_while_stmt(whilestmt),
            Stmt::Function(functionstmt) => visitor.visit_function_stmt(functionstmt),
            Stmt::Return(returnstmt) => visitor.visit_return_stmt(returnstmt),
            Stmt::Class(classtmt) => visitor.visit_class_stmt(classtmt),
        }
    }
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expr::Binary(binary_expr) => binary_expr.fmt(f),
            Expr::Grouping(grouping_expr) => grouping_expr.fmt(f),
            Expr::Unary(unary_expr) => unary_expr.fmt(f),
            Expr::Literal(literal_expr) => literal_expr.fmt(f),
            Expr::Variable(var_expr) => var_expr.fmt(f),
            Expr::Assign(assign_expr) => assign_expr.fmt(f),
            Expr::Logical(logical_expr) => logical_expr.fmt(f),
            Expr::Call(call_expr) => call_expr.fmt(f),
            Expr::Get(get_expr) => get_expr.fmt(f),
            Expr::Set(set_expr) => set_expr.fmt(f),
            Expr::This(this_expr) => this_expr.fmt(f),
            Expr::Super(super_expr) => super_expr.fmt(f),
        }
    }
}

impl fmt::Display for SuperExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}.{})", self.keyword.lexeme, self.method.lexeme)
    }
}

impl fmt::Display for ThisExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({})", self.keyword.lexeme)
    }
}

impl fmt::Display for SetExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}.{} = {})", self.object, self.name, self.value)
    }
}

impl fmt::Display for GetExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}.{})", self.object, self.name.lexeme)
    }
}

impl fmt::Display for CallExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} ({:?}))", self.callee, self.arguments)
    }
}

impl fmt::Display for BinaryExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} {} {})", self.operator.lexeme, self.left, self.right)
    }
}

// same as binary expr
impl fmt::Display for LogicalExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} {} {})", self.operator.lexeme, self.left, self.right)
    }
}

impl fmt::Display for GroupingExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(group {})", self.expression)
    }
}

impl fmt::Display for UnaryExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({} {})", self.operator.lexeme, self.right)
    }
}

impl fmt::Display for LiteralExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LiteralExpr::Boolean(b) => write!(f, "{}", b),
            LiteralExpr::Nil => write!(f, "nil"),
            LiteralExpr::Number(n) => write!(f, "{}", n),
            LiteralExpr::Str(s) => write!(f, "{}", s),
        }
    }
}

impl fmt::Display for VarExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(var {})", self.name.lexeme)
    }
}

impl fmt::Display for AssignExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(var `{}` = `{}`)", self.name.lexeme, self.value)
    }
}

#[cfg(test)]
mod tests {
    use crate::scanner::token::{Token, TokenType};

    use super::*;

    #[test]
    fn test_expr_display() {
        let expr = Expr::Binary(BinaryExpr::new(
            Box::new(Expr::Unary(UnaryExpr::new(
                Token::new(TokenType::Minus, (0, 1), "-".to_string(), 1),
                Box::new(Expr::Literal(LiteralExpr::Number(123.0))),
            ))),
            Token::new(TokenType::Star, (0, 1), "*".to_string(), 1),
            Box::new(Expr::Grouping(GroupingExpr::new(Box::new(Expr::Literal(
                LiteralExpr::Number(45.67),
            ))))),
        ));

        let result = format!("{}", expr);
        dbg!(&result);
        let expected_result = "(* (- 123) (group 45.67))";
        assert_eq!(result, expected_result);
    }
}
