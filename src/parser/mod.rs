use crate::{
    error::SyntaxError,
    interpreter::object::function::FunctionType,
    scanner::token::{Token, TokenType},
};

pub mod grammar;
use grammar::*;

#[derive(Debug)]
pub struct Parser {
    tokens: Vec<Token>,
    current: usize,
}

pub const MAX_PARAMS_ARGS_LEN: usize = 255;

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self { tokens, current: 0 }
    }

    pub fn parse(&mut self) -> Result<Vec<Stmt>, SyntaxError> {
        let mut statements = Vec::new();
        while !self.is_at_end() {
            let declaration = self.declaration()?;
            statements.push(declaration);
        }
        Ok(statements)
    }

    fn declaration(&mut self) -> Result<Stmt, SyntaxError> {
        let result = if self.match_types(&[TokenType::Var]) {
            self.var_declaration()
        } else if self.match_types(&[TokenType::Class]) {
            self.class_declaration()
        } else if self.match_types(&[TokenType::Fun]) {
            self.function(FunctionType::Function)
        } else {
            self.statement()
        };

        if result.is_err() {
            self.synchronize();
        }
        result
    }

    fn class_declaration(&mut self) -> Result<Stmt, SyntaxError> {
        let name = self.consume_identifier(
            "expected identifier".to_string(),
            "give a valid identifier name after `class` to define a class".to_string(),
        )?;

        let superclass = if self.match_types(&[TokenType::Less]) {
            let superclass_token = self.consume_identifier("expected superclass name".to_string(), format!("attempt at inheriting `{}` from a super class but no super class name was provided", name.lexeme))?;
            let superclass_expr = Expr::Variable(VarExpr::new(superclass_token));
            Some(superclass_expr)
        } else {
            None
        };

        self.consume(
            TokenType::LeftBrace,
            "expected `{`",
            "class body should be enclosed in `{` `}`",
        )?;

        let mut methods = vec![];
        while !self.check(&TokenType::RightBrace) {
            methods.push(self.function(FunctionType::Method)?);
        }

        self.consume(
            TokenType::RightBrace,
            "expected `}`",
            "class body should be enclosed in `{` `}`",
        )?;

        Ok(Stmt::Class(ClassStmt::new(name, superclass, methods)))
    }

    fn function(&mut self, func_type: FunctionType) -> Result<Stmt, SyntaxError> {
        let name = self.consume_identifier(
            format!("expected {} name", func_type),
            format!("{}s should be defined with a valid name", func_type),
        )?;

        self.consume(
            TokenType::LeftParen,
            &format!("expected `(` after {} name", func_type),
            &format!(
                "{}s' parameters (even if none) should be enclosed in `(` `)`",
                func_type
            ),
        )?;

        let mut parameters = vec![];
        if !self.check(&TokenType::RightParen) {
            self.check_params_len(parameters.len(), func_type)?;
            let param_name = self.consume_identifier(
                format!("expected {} name", func_type),
                format!("{}s should be defined with a valid name", func_type),
            )?;
            parameters.push(param_name);
            while self.match_types(&[TokenType::Comma]) {
                let extra_param_name = self.consume_identifier(
                    format!("expected {} name", func_type),
                    format!("{}s should be defined with a valid name", func_type),
                )?;
                parameters.push(extra_param_name);
            }
        }

        self.consume(
            TokenType::RightParen,
            "expected `)` after parameters",
            &format!(
                "{}s' parameters (even if none) should be enclosed in `(` `)`",
                func_type
            ),
        )?;

        self.consume(
            TokenType::LeftBrace,
            &format!("expected `{{` before {} body", func_type),
            &format!(
                "{} body must be defined inside a block statement, enclosed in `{{` `}}`",
                func_type
            ),
        )?;

        let body = self.block()?;

        Ok(Stmt::Function(FunctionStmt::new(name, parameters, body)))
    }

    fn var_declaration(&mut self) -> Result<Stmt, SyntaxError> {
        let variable_name = self.consume_identifier(
            "expected identifier".to_string(),
            "use a valid identifier name".to_string(),
        )?;

        let initializer = if self.match_types(&[TokenType::Equal]) {
            Some(self.expression()?)
        } else {
            None
        };

        self.consume(
            TokenType::Semicolon,
            "expected `;` after variable declaration",
            "insert the missing `;`",
        )?;

        Ok(Stmt::Variable(VarStmt::new(variable_name, initializer)))
    }

    fn statement(&mut self) -> Result<Stmt, SyntaxError> {
        if self.match_types(&[TokenType::LeftBrace]) {
            Ok(Stmt::Block(BlockStmt::new(self.block()?)))
        } else if self.match_types(&[TokenType::Print]) {
            self.print_statement()
        } else if self.match_types(&[TokenType::If]) {
            self.if_statement()
        } else if self.match_types(&[TokenType::While]) {
            self.while_statement()
        } else if self.match_types(&[TokenType::For]) {
            self.for_statement()
        } else if self.match_types(&[TokenType::Return]) {
            self.return_statement()
        } else {
            self.expr_statement()
        }
    }

    fn return_statement(&mut self) -> Result<Stmt, SyntaxError> {
        let keyword = self.previous();
        // if there's a semicolon immediately following `return` then return nil
        let value = if self.check(&TokenType::Semicolon) {
            Expr::Literal(LiteralExpr::Nil)
        } else {
            self.expression()?
        };

        self.consume(
            TokenType::Semicolon,
            "expected `;` after return value",
            "insert the missing `;`",
        )?;

        Ok(Stmt::Return(ReturnStmt::new(keyword, value)))
    }

    fn for_statement(&mut self) -> Result<Stmt, SyntaxError> {
        self.consume(
            TokenType::LeftParen,
            "expected `(` after `for`",
            "`for` loop initializer, condition and increment must be enclosed in `(` `)`",
        )?;

        let initializer = if self.match_types(&[TokenType::Semicolon]) {
            None
        } else if self.match_types(&[TokenType::Var]) {
            Some(self.var_declaration()?)
        } else {
            Some(self.expr_statement()?)
        };

        let condition = if !self.check(&TokenType::Semicolon) {
            self.expression()?
        } else {
            Expr::Literal(LiteralExpr::Boolean(true))
        };

        self.consume(
            TokenType::Semicolon,
            "expected `;` after loop condition `for`",
            "`for` loop initializer and condition must be delimeted by `;`",
        )?;

        let increment = if !self.check(&TokenType::RightParen) {
            Some(self.expression()?)
        } else {
            None
        };

        self.consume(
            TokenType::RightParen,
            "expected `)` after `for` clauses",
            "`for` loop initializer, condition and increment must be enclosed in `(` `)`",
        )?;

        let mut body = self.statement()?;

        if let Some(increment) = increment {
            let bodystmts = vec![body, Stmt::Expression(increment)];
            body = Stmt::Block(BlockStmt::new(bodystmts));
        }

        body = Stmt::While(WhileStmt::new(condition, Box::new(body)));

        if let Some(initializer) = initializer {
            let bodystmts = vec![initializer, body];
            body = Stmt::Block(BlockStmt::new(bodystmts));
        }

        Ok(body)
    }

    fn while_statement(&mut self) -> Result<Stmt, SyntaxError> {
        self.consume(
            TokenType::LeftParen,
            "expected `(` after `while`",
            "conditions in a while statement must be enclosed in `(` `)`",
        )?;
        let condition = self.expression()?;
        self.consume(
            TokenType::RightParen,
            "expected `)` after `while`",
            "conditions in a while statement must be enclosed in `(` `)`",
        )?;
        let body = self.statement()?;
        Ok(Stmt::While(WhileStmt::new(condition, Box::new(body))))
    }

    fn if_statement(&mut self) -> Result<Stmt, SyntaxError> {
        self.consume(
            TokenType::LeftParen,
            "expected `(` after `if`",
            "conditions in an if statement must be enclosed in `(` `)`",
        )?;
        let condition = self.expression()?;
        self.consume(
            TokenType::RightParen,
            "expected `)` after `if`",
            "conditions in an if statement must be enclosed in `(` `)`",
        )?;

        let then_branch = self.statement()?;
        let else_branch = if self.match_types(&[TokenType::Else]) {
            Some(Box::new(self.statement()?))
        } else {
            None
        };

        Ok(Stmt::If(IfStmt::new(
            condition,
            Box::new(then_branch),
            else_branch,
        )))
    }

    fn block(&mut self) -> Result<Vec<Stmt>, SyntaxError> {
        let mut statements = vec![];
        while !self.check(&TokenType::RightBrace) {
            statements.push(self.declaration()?);
        }
        self.consume(
            TokenType::RightBrace,
            "expected `}` after block",
            "the delimeter might not be properly closed",
        )?;
        Ok(statements)
    }

    fn print_statement(&mut self) -> Result<Stmt, SyntaxError> {
        // we already matched and consumed the `print` token above the grammar, so no
        // need to do it here
        let value = self.expression()?;
        self.consume(
            TokenType::Semicolon,
            "expected `;` after expression",
            "add the missing `;`",
        )?;
        Ok(Stmt::Print(value))
    }

    fn expr_statement(&mut self) -> Result<Stmt, SyntaxError> {
        // we already matched and consumed the `print` token above the grammar, so no
        // need to do it here
        let value = self.expression()?;
        self.consume(
            TokenType::Semicolon,
            "expected `;` after expression",
            "add the missing `;`",
        )?;
        Ok(Stmt::Expression(value))
    }

    fn expression(&mut self) -> Result<Expr, SyntaxError> {
        self.assignment()
    }

    fn assignment(&mut self) -> Result<Expr, SyntaxError> {
        let expr = self.or()?;
        if self.match_types(&[TokenType::Equal]) {
            let equals = self.previous();
            let value = self.assignment()?;
            if let Expr::Get(get_expr) = expr {
                let set_expr = SetExpr::new(get_expr.object, get_expr.name, Box::new(value));
                return Ok(Expr::Set(set_expr));
            } else if let Expr::Variable(var) = expr {
                return Ok(Expr::Assign(AssignExpr::new(var.name, Box::new(value))));
            } else {
                // TODO: Add source to Token type so that we can do proper error
                // handling here, might need to use Arc since the
                // source will need to be cloned many times.
                // Also, figure out how to print this error without having to
                // bubble it back to main().
                let err = SyntaxError::new(
                    &equals,
                    "invalid assignment target".to_string(),
                    format!("cannot assign to `{}` to `{}`", value, expr),
                );
                eprintln!("{:?}", err);
            }
        }
        Ok(expr)
    }

    fn or(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.and()?;
        while self.match_types(&[TokenType::Or]) {
            let operator = self.previous();
            let right = self.and()?;
            expr = Expr::Logical(LogicalExpr::new(Box::new(expr), operator, Box::new(right)));
        }
        Ok(expr)
    }

    fn and(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.equality()?;
        while self.match_types(&[TokenType::And]) {
            let operator = self.previous();
            let right = self.equality()?;
            expr = Expr::Logical(LogicalExpr::new(Box::new(expr), operator, Box::new(right)));
        }
        Ok(expr)
    }

    fn equality(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.comparison()?;
        while self.match_types(&[TokenType::BangEqual, TokenType::EqualEqual]) {
            let operator = self.previous();
            let right = self.comparison()?;
            expr = Expr::Binary(BinaryExpr {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            });
        }
        Ok(expr)
    }

    fn comparison(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.term()?;
        while self.match_types(&[
            TokenType::Greater,
            TokenType::GreaterEqual,
            TokenType::Less,
            TokenType::LessEqual,
        ]) {
            let operator = self.previous();
            let right = self.term()?;
            expr = Expr::Binary(BinaryExpr {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            })
        }
        Ok(expr)
    }

    fn term(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.factor()?;
        while self.match_types(&[TokenType::Plus, TokenType::Minus]) {
            let operator = self.previous();
            let right = self.factor()?;
            expr = Expr::Binary(BinaryExpr {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            })
        }
        Ok(expr)
    }

    fn factor(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.unary()?;
        while self.match_types(&[TokenType::Slash, TokenType::Star]) {
            let operator = self.previous();
            let right = self.unary()?;
            expr = Expr::Binary(BinaryExpr {
                left: Box::new(expr),
                operator,
                right: Box::new(right),
            })
        }
        Ok(expr)
    }

    fn unary(&mut self) -> Result<Expr, SyntaxError> {
        if self.match_types(&[TokenType::Bang, TokenType::Minus]) {
            let operator = self.previous();
            let right = self.unary()?;
            Ok(Expr::Unary(UnaryExpr {
                operator,
                right: Box::new(right),
            }))
        } else {
            self.call()
        }
    }

    fn call(&mut self) -> Result<Expr, SyntaxError> {
        let mut expr = self.primary()?;

        loop {
            if self.match_types(&[TokenType::LeftParen]) {
                expr = self.finish_call(expr)?;
            } else if self.match_types(&[TokenType::Dot]) {
                let name = self.consume_identifier(
                    "expected identifier after `.`".to_string(),
                    "specify a property name after the `.` expression".to_string(),
                )?;
                expr = Expr::Get(GetExpr::new(Box::new(expr), name));
            } else {
                break;
            }
        }

        Ok(expr)
    }

    fn primary(&mut self) -> Result<Expr, SyntaxError> {
        use TokenType::*;
        let current_token = self.peek();
        let expr = match &current_token.ty {
            False => Expr::Literal(LiteralExpr::Boolean(false)),
            True => Expr::Literal(LiteralExpr::Boolean(true)),
            Nil => Expr::Literal(LiteralExpr::Nil),
            Number(num_val) => Expr::Literal(LiteralExpr::Number(*num_val)),
            Str(str_val) => Expr::Literal(LiteralExpr::Str(str_val.clone())),
            LeftParen => {
                let _ = self.advance();
                let expr = self.expression()?;
                let _ = self.consume(
                    RightParen,
                    "expected ')' after expression",
                    "add the missing ')'",
                )?;
                // doing this because we'll be doing self.advance() once again later
                self.current -= 1;
                Expr::Grouping(GroupingExpr {
                    expression: Box::new(expr),
                })
            }
            Identifier(_) => Expr::Variable(VarExpr::new(current_token.clone())),
            This => Expr::This(ThisExpr::new(current_token.clone())),
            Super => {
                let keyword = current_token.clone();

                self.current += 1;

                self.consume(
                    TokenType::Dot,
                    "expected `.` after `super`",
                    "you can't use `super` without a method access",
                )?;

                let method = self.consume_identifier(
                    "expected superclass method name".to_string(),
                    "`super` expressions should access a valid method from the superclass"
                        .to_string(),
                )?;

                return Ok(Expr::Super(SuperExpr::new(keyword, method)));
            }
            invalid => {
                return Err(SyntaxError {
                    line: current_token.line,
                    span: current_token.span.into(),
                    err_message: "expected expression".to_string(),
                    help_message: format!("expected expression, found `{}`", invalid),
                });
            }
        };

        let _ = self.advance();
        Ok(expr)
    }

    fn finish_call(&mut self, callee: Expr) -> Result<Expr, SyntaxError> {
        let mut arguments = vec![];
        if !self.check(&TokenType::RightParen) {
            self.check_args_len(arguments.len())?;
            arguments.push(self.expression()?);
            while self.match_types(&[TokenType::Comma]) {
                self.check_args_len(arguments.len())?;
                arguments.push(self.expression()?);
            }
        }

        let closing_paren = self.consume(
            TokenType::RightParen,
            "expected `)` after arguments",
            "arguments (even if none) in a function call should be enclosed in `(` `)`",
        )?;

        Ok(Expr::Call(CallExpr::new(
            Box::new(callee),
            closing_paren,
            arguments,
        )))
    }

    fn check_args_len(&self, args_len: usize) -> Result<(), SyntaxError> {
        // upper limit for number of arguments
        if args_len >= MAX_PARAMS_ARGS_LEN {
            // we already have MAX_PARAMS_ARGS_LEN arguments and they're trying to add one
            // more to the args list
            let err = SyntaxError::new(
                self.peek(),
                "too many arguments".to_string(),
                format!(
                    "function calls can't have more than {} arguments",
                    MAX_PARAMS_ARGS_LEN
                ),
            );
            // TODO: we should only report the error and not return it here
            return Err(err);
        }
        Ok(())
    }

    fn check_params_len(
        &self,
        params_len: usize,
        func_type: FunctionType,
    ) -> Result<(), SyntaxError> {
        // upper limit for number of parameters
        if params_len >= MAX_PARAMS_ARGS_LEN {
            // we already have MAX_PARAMS_ARGS_LEN parameters and they're trying to add one
            // more to the params list
            let err = SyntaxError::new(
                self.peek(),
                "too many parameters".to_string(),
                format!(
                    "{}s can't define more than {} parameters",
                    func_type, MAX_PARAMS_ARGS_LEN
                ),
            );
            // TODO: we should only report the error and not return it here
            return Err(err);
        }
        Ok(())
    }

    // discards token until we find a statement boundary
    fn synchronize(&mut self) {
        let _ = self.advance();
        while !self.is_at_end() {
            if self.previous().ty == TokenType::Semicolon {
                return;
            }
            match self.peek().ty {
                TokenType::Class
                | TokenType::Fun
                | TokenType::Var
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Return => {
                    return;
                }
                _ => {
                    let _ = self.advance();
                }
            }
        }
    }

    fn consume(
        &mut self,
        ty: TokenType,
        err_msg: &str,
        help_msg: &str,
    ) -> Result<Token, SyntaxError> {
        if self.check(&ty) {
            Ok(self.advance())
        } else {
            let token = self.peek();
            Err(SyntaxError {
                line: token.line,
                span: token.span.into(),
                err_message: err_msg.to_string(),
                help_message: help_msg.to_string(),
            })
        }
    }

    fn consume_identifier(
        &mut self,
        err_msg: String,
        help_msg: String,
    ) -> Result<Token, SyntaxError> {
        let current_token = self.peek();
        match current_token.ty {
            TokenType::Identifier(_) => Ok(self.advance()),
            _ => Err(SyntaxError::new(current_token, err_msg, help_msg)),
        }
    }

    fn match_types(&mut self, types: &[TokenType]) -> bool {
        for token_type in types {
            if self.check(token_type) {
                self.advance();
                return true;
            }
        }
        false
    }

    fn check(&self, token_type: &TokenType) -> bool {
        if self.is_at_end() {
            return false;
        }
        self.peek().ty == *token_type
    }

    fn advance(&mut self) -> Token {
        if !self.is_at_end() {
            self.current += 1;
        }
        self.previous()
    }

    fn is_at_end(&self) -> bool {
        self.peek().ty == TokenType::Eof
    }

    fn peek(&self) -> &Token {
        &self.tokens[self.current]
    }

    fn previous(&self) -> Token {
        self.tokens[self.current - 1].clone()
    }
}
