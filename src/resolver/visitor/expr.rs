use crate::{
    error::RuntimeError,
    interpreter::{object::class::ClassType, RuntimeReturnValue},
    parser::grammar::ExprVisitor,
    resolver::Resolver,
};

impl<'i> ExprVisitor<(), RuntimeReturnValue> for Resolver<'i> {
    fn visit_var_expr(
        &mut self,
        value: &crate::parser::grammar::VarExpr,
    ) -> Result<(), RuntimeReturnValue> {
        if let Some(scope) = self.scopes.last() {
            if let Some(flag) = scope.get(&value.name.lexeme) {
                if !(*flag) {
                    return Err(RuntimeReturnValue::Error(RuntimeError::new(
                        &value.name,
                        "can't read local variable in its own initializer".to_string(),
                        "".to_string(),
                    )));
                }
            }
        }
        self.resolve_local(&value.name);
        Ok(())
    }

    fn visit_assign_expr(
        &mut self,
        value: &crate::parser::grammar::AssignExpr,
    ) -> Result<(), RuntimeReturnValue> {
        value.value.accept(self)?;
        self.resolve_local(&value.name);
        Ok(())
    }

    fn visit_binary_expr(
        &mut self,
        expr: &crate::parser::grammar::BinaryExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.left)?;
        self.resolve_expr(&expr.right)?;
        Ok(())
    }

    fn visit_call_expr(
        &mut self,
        expr: &crate::parser::grammar::CallExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.callee)?;

        expr.arguments
            .iter()
            .try_for_each(|argument| self.resolve_expr(argument))?;

        Ok(())
    }

    fn visit_grouping_expr(
        &mut self,
        expr: &crate::parser::grammar::GroupingExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.expression)
    }

    fn visit_literal_expr(
        &mut self,
        _value: &crate::parser::grammar::LiteralExpr,
    ) -> Result<(), RuntimeReturnValue> {
        Ok(())
    }

    fn visit_logical_expr(
        &mut self,
        expr: &crate::parser::grammar::LogicalExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.left)?;
        self.resolve_expr(&expr.right)?;
        Ok(())
    }

    fn visit_unary_expr(
        &mut self,
        expr: &crate::parser::grammar::UnaryExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.right)
    }

    fn visit_get_expr(
        &mut self,
        expr: &crate::parser::grammar::GetExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.object)?;
        Ok(())
    }

    fn visit_set_expr(
        &mut self,
        expr: &crate::parser::grammar::SetExpr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&expr.value)?;
        self.resolve_expr(&expr.object)?;
        Ok(())
    }

    fn visit_this_expr(
        &mut self,
        expr: &crate::parser::grammar::ThisExpr,
    ) -> Result<(), RuntimeReturnValue> {
        if self.current_class == ClassType::None {
            return Err(RuntimeReturnValue::Error(RuntimeError::new(
                &expr.keyword,
                "invalid use of `this` expression".to_string(),
                "can't use `this` outside of a class' method".to_string(),
            )));
        }
        self.resolve_local(&expr.keyword);
        Ok(())
    }

    fn visit_super_expr(
        &mut self,
        expr: &crate::parser::grammar::SuperExpr,
    ) -> Result<(), RuntimeReturnValue> {
        match self.current_class {
            ClassType::None => Err(RuntimeError::new(
                &expr.keyword,
                "invalid use of `super`".to_string(),
                "can't use `super` outside of a class".to_string(),
            )
            .into()),
            ClassType::Class => Err(RuntimeError::new(
                &expr.keyword,
                "invalid use of `super`".to_string(),
                "can't use `super` in a class with no superclass".to_string(),
            )
            .into()),
            _ => {
                self.resolve_local(&expr.keyword);
                Ok(())
            }
        }
    }
}
