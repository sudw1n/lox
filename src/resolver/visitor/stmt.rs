use std::mem;

use crate::{
    error::RuntimeError,
    interpreter::{object::class::ClassType, RuntimeReturnValue},
    parser::grammar::{Expr, Stmt, StmtVisitor},
    resolver::{FunctionType, Resolver},
};

impl<'i> StmtVisitor<(), RuntimeReturnValue> for Resolver<'i> {
    fn visit_block_stmt(
        &mut self,
        stmt: &crate::parser::grammar::BlockStmt,
    ) -> Result<(), RuntimeReturnValue> {
        self.begin_scope();
        self.resolve_stmts(&stmt.statements)?;
        self.end_scope();
        Ok(())
    }

    fn visit_var_stmt(
        &mut self,
        stmt: &crate::parser::grammar::VarStmt,
    ) -> Result<(), RuntimeReturnValue> {
        self.declare(&stmt.name)?;
        if let Some(initializer) = &stmt.initializer {
            self.resolve_expr(initializer)?;
        }
        self.define(&stmt.name);
        Ok(())
    }

    fn visit_function_stmt(
        &mut self,
        stmt: &crate::parser::grammar::FunctionStmt,
    ) -> Result<(), RuntimeReturnValue> {
        self.declare(&stmt.name)?;
        self.define(&stmt.name);

        self.resolve_function(stmt, crate::resolver::FunctionType::Function)?;
        Ok(())
    }

    fn visit_expression_stmt(
        &mut self,
        expr: &crate::parser::grammar::Expr,
    ) -> Result<(), RuntimeReturnValue> {
        expr.accept(self)
    }

    fn visit_if_stmt(
        &mut self,
        stmt: &crate::parser::grammar::IfStmt,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&stmt.condition)?;
        self.resolve_stmt(&stmt.then_branch)?;
        if let Some(else_branch) = &stmt.else_branch {
            self.resolve_stmt(else_branch)?;
        }
        Ok(())
    }

    fn visit_print_stmt(
        &mut self,
        expr: &crate::parser::grammar::Expr,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(expr)
    }

    fn visit_return_stmt(
        &mut self,
        stmt: &crate::parser::grammar::ReturnStmt,
    ) -> Result<(), RuntimeReturnValue> {
        match self.current_function {
            FunctionType::None => Err(RuntimeReturnValue::Error(RuntimeError::new(
                &stmt.keyword,
                "expected item, found keyword `return`".to_string(),
                "can't return from top-level code".to_string(),
            ))),
            FunctionType::Initializer => Err(RuntimeReturnValue::Error(RuntimeError::new(
                &stmt.keyword,
                "invalid use of keyword `return`".to_string(),
                "can't return a value from an initializer".to_string(),
            ))),
            _ => self.resolve_expr(&stmt.value),
        }
    }

    fn visit_while_stmt(
        &mut self,
        stmt: &crate::parser::grammar::WhileStmt,
    ) -> Result<(), RuntimeReturnValue> {
        self.resolve_expr(&stmt.condition)?;
        self.resolve_stmt(&stmt.body)?;
        Ok(())
    }

    fn visit_class_stmt(
        &mut self,
        stmt: &crate::parser::grammar::ClassStmt,
    ) -> Result<(), RuntimeReturnValue> {
        let enclosing_class = mem::replace(&mut self.current_class, ClassType::Class);

        self.declare(&stmt.name)?;
        self.define(&stmt.name);

        if let Some(Expr::Variable(superclass)) = &stmt.superclass {
            if stmt.name.lexeme == superclass.name.lexeme {
                return Err(RuntimeReturnValue::Error(RuntimeError::new(
                    &superclass.name,
                    "invalid attempt at inheritance: superclass and subclass are the same"
                        .to_string(),
                    "a class can't inherit from itself".to_string(),
                )));
            }
            self.current_class = ClassType::SubClass;
            self.resolve_local(&superclass.name);

            self.begin_scope();
            self.scopes
                .last_mut()
                .expect("should have a scope since we just created a new one with begin_scope()")
                .insert("super".to_string(), true);
        }

        self.begin_scope();
        self.scopes
            .last_mut()
            .expect("scopes shouldn't be empty since we just did begin_scope()")
            .insert("this".to_string(), true);

        stmt.methods.iter().try_for_each(|class_function| {
            if let Stmt::Function(method) = class_function {
                let declaration = if method.name.lexeme.eq("init") {
                    FunctionType::Initializer
                } else {
                    FunctionType::Method
                };
                self.resolve_function(method, declaration)
            } else {
                Ok(())
            }
        })?;

        if stmt.superclass.is_some() {
            self.end_scope();
        }

        self.end_scope();

        self.current_class = enclosing_class;

        Ok(())
    }
}
