use std::collections::HashMap;

use crate::{
    error::RuntimeError,
    interpreter::{
        object::{class::ClassType, function::FunctionType},
        Interpreter, RuntimeReturnValue,
    },
    parser::grammar::{Expr, FunctionStmt, Stmt},
    scanner::token::Token,
};
pub mod visitor;

pub struct Resolver<'i> {
    interpreter: &'i mut Interpreter,
    scopes: Vec<HashMap<String, bool>>,
    current_function: FunctionType,
    current_class: ClassType,
}

impl<'i> Resolver<'i> {
    pub fn new(interpreter: &'i mut Interpreter) -> Self {
        Self {
            interpreter,
            scopes: vec![],
            current_function: FunctionType::default(),
            current_class: ClassType::default(),
        }
    }

    #[inline(always)]
    pub fn resolve_stmts(&mut self, stmts: &[Stmt]) -> Result<(), RuntimeReturnValue> {
        stmts.iter().try_for_each(|stmt| stmt.accept(self))
    }

    #[inline(always)]
    fn resolve_expr(&mut self, expr: &Expr) -> Result<(), RuntimeReturnValue> {
        expr.accept(self)
    }

    #[inline(always)]
    fn resolve_stmt(&mut self, stmt: &Stmt) -> Result<(), RuntimeReturnValue> {
        stmt.accept(self)
    }

    fn begin_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }

    fn end_scope(&mut self) {
        self.scopes.pop();
    }

    fn declare(&mut self, name: &Token) -> Result<(), RuntimeReturnValue> {
        match self.scopes.last_mut() {
            Some(scope) => {
                if scope.contains_key(&name.lexeme) {
                    return Err(RuntimeReturnValue::Error(RuntimeError::new(
                        name,
                        "duplicate declaration".to_string(),
                        format!(
                            "the variable {} has already been declared in this scope",
                            name.lexeme
                        ),
                    )));
                }
                scope.insert(name.lexeme.clone(), false);
                Ok(())
            }
            None => Ok(()),
        }
    }

    fn define(&mut self, name: &Token) {
        if let Some(ref mut scope) = self.scopes.last_mut() {
            scope.insert(name.lexeme.clone(), true);
        }
    }

    fn resolve_local(&mut self, name: &Token) {
        for (i, scope) in self.scopes.iter().rev().enumerate() {
            if scope.contains_key(&name.lexeme) {
                self.interpreter.resolve(name, i);
                return;
            }
        }
    }

    fn resolve_function(
        &mut self,
        function: &FunctionStmt,
        func_type: FunctionType,
    ) -> Result<(), RuntimeReturnValue> {
        debug_assert!(
            func_type != FunctionType::None,
            "[in lox::resolver::resolve_function()] a function was given to resolve but it is FunctionType::None"
        );
        let enclosing_function = self.current_function;
        self.current_function = func_type;
        self.begin_scope();
        for param in &function.params {
            self.declare(param)?;
            self.define(param);
        }
        self.resolve_stmts(&function.body)?;
        self.end_scope();
        self.current_function = enclosing_function;
        Ok(())
    }
}
