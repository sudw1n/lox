use std::io;

use miette::{Diagnostic, SourceSpan};
use thiserror::Error;

use crate::scanner::token::Token;

#[derive(Error, Diagnostic, Debug)]
pub enum LoxError {
    #[error(transparent)]
    #[diagnostic(code(lox_error::io_error))]
    IOError(#[from] io::Error),

    #[diagnostic(code(lox_error))]
    #[error("execution failed because of syntax errors")]
    SyntaxErrors(#[related] Vec<SyntaxError>),

    #[diagnostic(code(lox_error))]
    #[error("execution failed because of runtime errors")]
    RuntimeErrors(#[related] Vec<RuntimeError>),
}

#[derive(Error, Diagnostic, Clone, Debug)]
#[diagnostic(code(lox_error::syntax_error))]
#[error("syntax error: {err_message} at line {line}")]
pub struct SyntaxError {
    pub line: usize,
    #[label("here")]
    pub span: SourceSpan,
    pub err_message: String,
    #[help]
    pub help_message: String,
}

impl SyntaxError {
    pub fn new(token: &Token, err: String, help: String) -> Self {
        SyntaxError {
            line: token.line,
            span: token.span.into(),
            err_message: err,
            help_message: help,
        }
    }
}

#[derive(Error, Diagnostic, Clone, Debug)]
#[diagnostic(code(lox_error::runtime_error))]
#[error("runtime error: {err_message} at line {line}")]
pub struct RuntimeError {
    line: usize,
    #[label("here")]
    span: SourceSpan,
    err_message: String,
    #[help]
    help_message: String,
}

impl RuntimeError {
    pub fn new(token: &Token, err: String, help: String) -> Self {
        Self {
            line: token.line,
            span: token.span.into(),
            err_message: err,
            help_message: help,
        }
    }
}
