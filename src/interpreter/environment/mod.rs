//! Implements lexical scoping for the interpreter by providing an `Environment`
//! struct to define, query and assign variables
use std::{cell::RefCell, collections::HashMap, rc::Rc};

use crate::{error::RuntimeError, scanner::token::Token};

use super::{object::Object, RuntimeReturnValue};

#[derive(Debug, Clone, PartialEq)]
pub struct Environment {
    // to hold environment from current scope
    pub values: HashMap<String, Object>,
    // to hold environment from outer scope
    pub enclosing: Option<Rc<RefCell<Environment>>>,
}

impl Environment {
    pub fn new() -> Self {
        Self {
            values: HashMap::new(),
            enclosing: None,
        }
    }

    pub fn from(enclosing: &Rc<RefCell<Environment>>) -> Self {
        Self {
            values: HashMap::new(),
            enclosing: Some(Rc::clone(enclosing)),
        }
    }

    pub fn get(&self, name: &Token) -> Result<Object, RuntimeReturnValue> {
        if let Some(value) = self.values.get(&name.lexeme) {
            Ok(value.clone())
        } else if let Some(enclosing) = &self.enclosing {
            enclosing.borrow().get(name)
        } else {
            Err(RuntimeReturnValue::Error(RuntimeError::new(
                name,
                "undefined variable".to_string(),
                format!(
                    "couldn't find variable `{}` in the current scope",
                    &name.lexeme
                ),
            )))
        }
    }

    pub fn get_at(&self, distance: usize, name: &str) -> Option<Object> {
        if distance > 0 {
            if let Some(value) = self.ancestor(distance).borrow().values.get(name) {
                return Some(value.clone());
            }
        } else if let Some(value) = self.values.get(name) {
            return Some(value.clone());
        }
        None
    }

    pub fn assign_at(
        &mut self,
        distance: usize,
        name: &Token,
        value: &Object,
    ) -> Result<(), RuntimeReturnValue> {
        if distance > 0 {
            if let Some(old_value) = self
                .ancestor(distance)
                .borrow_mut()
                .values
                .get_mut(&name.lexeme)
            {
                *old_value = value.clone();
                return Ok(());
            }
        } else {
            if let Some(old_value) = self.values.get_mut(&name.lexeme) {
                *old_value = value.clone();
            }
            return Ok(());
        };

        Err(RuntimeReturnValue::Error(RuntimeError::new(
            name,
            "undefined assignment target".to_string(),
            format!(
                "couldn't find variable `{}` in the current scope",
                name.lexeme
            ),
        )))
    }

    fn ancestor(&self, distance: usize) -> Rc<RefCell<Environment>> {
        // get first parent environment
        let parent = self
            .enclosing
            .clone()
            .expect("internal error: no enclosing environment found at the first parent");
        let mut environment = Rc::clone(&parent);

        // get next parents
        for i in 1..distance {
            let parent = environment.borrow().enclosing.clone().unwrap_or_else(|| {
                panic!(
                    "internal error: no enclosing environment found at distance {}",
                    i
                )
            });
            environment = Rc::clone(&parent);
        }

        environment
    }

    pub fn insert(&mut self, key: String, value: Object) -> Option<Object> {
        self.values.insert(key, value)
    }

    pub fn assign(&mut self, name: &Token, new_value: &Object) -> Result<(), RuntimeReturnValue> {
        if let Some(old_value) = self.values.get_mut(&name.lexeme) {
            *old_value = new_value.clone();
        // if the variable being assigned to isn't in the current scope, then
        // try to assign in the outer scope
        } else if let Some(enclosing) = &self.enclosing {
            enclosing.borrow_mut().assign(name, new_value)?;
        } else {
            return Err(RuntimeReturnValue::Error(RuntimeError::new(
                name,
                "undefined assignment target".to_string(),
                format!(
                    "couldn't find variable `{}` in the current scope",
                    name.lexeme
                ),
            )));
        }
        Ok(())
    }
}

impl Default for Environment {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::scanner::token::TokenType;

    #[test]
    fn test_lookup() {
        let mut environment = Environment::new();
        let x = Token::new(
            TokenType::Identifier("x".to_string()),
            (0, 1),
            "x".to_string(),
            1,
        );
        let x_value = Object::Number(10.1);
        environment.insert(x.lexeme.clone(), x_value.clone());
        assert_eq!(environment.get(&x).unwrap(), x_value);
    }

    #[test]
    fn test_assign() {
        let mut environment = Environment::new();

        let x = Token::new(
            TokenType::Identifier("x".to_string()),
            (0, 1),
            "x".to_string(),
            1,
        );
        let initial_value = Object::Number(10.1);

        environment.insert(x.lexeme.clone(), initial_value);

        let new_value = Object::Number(20.0);
        environment.assign(&x, &new_value).unwrap();

        assert_eq!(environment.get(&x).unwrap(), new_value);
    }

    #[test]
    fn test_undefined() {
        let environment = Environment::new();
        let x = Token::new(
            TokenType::Identifier("x".to_string()),
            (0, 1),
            "x".to_string(),
            1,
        );
        // we don't define the variable
        // environment.insert(x.lexeme.clone(), x_value.clone());

        // so this should error
        assert!(environment.get(&x).is_err());
    }

    #[test]
    fn test_enclosing_insert() {
        let outer = Rc::new(RefCell::new(Environment::new()));
        let inner = Environment::from(&outer);

        let x = Token::new(
            TokenType::Identifier("x".to_string()),
            (0, 1),
            "x".to_string(),
            1,
        );

        let x_val = Object::Number(10.1);

        // define the variable in the outer scope
        outer.borrow_mut().insert(x.lexeme.clone(), x_val.clone());

        // we should be able to find the variable in the inner scope since it's still
        // defined in the outer scope
        assert_eq!(inner.get(&x).unwrap(), x_val);
    }

    #[test]
    fn test_enclosing_assign() {
        let outer = Rc::new(RefCell::new(Environment::new()));
        let mut inner = Environment::from(&outer);

        let x = Token::new(
            TokenType::Identifier("x".to_string()),
            (0, 1),
            "x".to_string(),
            1,
        );

        let old_value = Object::Number(10.1);

        // define the variable in the outer scope
        outer
            .borrow_mut()
            .insert(x.lexeme.clone(), old_value.clone());

        let new_value = Object::Boolean(true);
        // assign a new value to the variable in the outer scope
        // from an inner scope
        inner.assign(&x, &new_value).unwrap();

        // the variable in the outer scope should have been changed
        assert_ne!(inner.get(&x).unwrap(), old_value);
        assert_eq!(inner.get(&x).unwrap(), new_value);
    }
}
