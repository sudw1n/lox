use std::time::{SystemTime, UNIX_EPOCH};

use super::{
    environment::Environment,
    object::{function::LoxFunction, Object},
};

pub fn define_clock(globals: &mut Environment) {
    let clock = |_objects: &[Object]| -> Object {
        Object::Number(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("couldn't retrieve time")
                .as_millis() as f64,
        )
    };
    globals.insert(
        "clock".to_string(),
        Object::Callable(LoxFunction::Native {
            arity: 0,
            body: Box::new(clock),
        }),
    );
}
