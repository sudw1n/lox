use std::{cell::RefCell, rc::Rc};

use crate::{
    error::RuntimeError,
    interpreter::{
        object::{class::LoxInstance, Object},
        Interpreter, RuntimeReturnValue,
    },
    parser::grammar::{
        AssignExpr, BinaryExpr, CallExpr, ExprVisitor, GetExpr, GroupingExpr, LiteralExpr,
        LogicalExpr, SetExpr, SuperExpr, ThisExpr, UnaryExpr, VarExpr,
    },
    scanner::token::{Token, TokenType},
};

use super::is_truthy;

impl ExprVisitor<Object, RuntimeReturnValue> for Interpreter {
    fn visit_literal_expr(&mut self, value: &LiteralExpr) -> Result<Object, RuntimeReturnValue> {
        match value {
            LiteralExpr::Nil => Ok(Object::Nil),
            LiteralExpr::Boolean(b) => Ok(Object::Boolean(*b)),
            LiteralExpr::Number(n) => Ok(Object::Number(*n)),
            LiteralExpr::Str(s) => Ok(Object::Str(s.clone())),
        }
    }

    fn visit_grouping_expr(&mut self, expr: &GroupingExpr) -> Result<Object, RuntimeReturnValue> {
        self.evaluate(&expr.expression)
    }

    fn visit_unary_expr(&mut self, expr: &UnaryExpr) -> Result<Object, RuntimeReturnValue> {
        let right = self.evaluate(&expr.right)?;
        let operator = &expr.operator;
        match operator.ty {
            // we have implemented std::ops for Object, where doing these arithmetic operations may
            // also return an error
            TokenType::Minus => (-right).map_err(|e| {
                RuntimeReturnValue::Error(RuntimeError::new(
                    operator,
                    e.to_string(),
                    "try converting the type to `number`".to_string(),
                ))
            }),
            // however, performing `!` won't have to error out
            TokenType::Bang => Ok(!right),
            _ => Err(RuntimeReturnValue::Error(RuntimeError::new(
                operator,
                format!("unexpected operator `{}`", operator.lexeme),
                "only `!` and `-` are allowed as unary operators, if that's what you intended"
                    .to_string(),
            ))),
        }
    }

    fn visit_binary_expr(&mut self, expr: &BinaryExpr) -> Result<Object, RuntimeReturnValue> {
        let left = self.evaluate(&expr.left)?;
        let right = self.evaluate(&expr.right)?;

        let left_ty = left.display();
        let right_ty = right.display();
        let operator = &expr.operator;
        match operator.ty {
            // Although we have implemented `std::cmp::PartialOrd` for `Object`, the implementation
            // will simply return false if the two operands are of different variants. However, we
            // need to detect this.
            TokenType::Greater
            | TokenType::GreaterEqual
            | TokenType::Less
            | TokenType::LessEqual => {
                if let (Object::Number(left_num), Object::Number(right_num)) = (&left, &right) {
                    match operator.ty {
                        TokenType::Greater => Ok(Object::Boolean(left_num > right_num)),
                        TokenType::GreaterEqual => Ok(Object::Boolean(left_num >= right_num)),
                        TokenType::Less => Ok(Object::Boolean(left_num < right_num)),
                        TokenType::LessEqual => Ok(Object::Boolean(left_num <= right_num)),
                        TokenType::BangEqual => Ok(Object::Boolean(left_num != right_num)),
                        TokenType::EqualEqual => Ok(Object::Boolean(left_num == right_num)),
                        _ => unreachable!(),
                    }
                } else {
                    Err(report_binary_expr_error(left_ty, operator, right_ty, None))
                }
            }
            TokenType::EqualEqual => Ok(Object::Boolean(left == right)),
            TokenType::BangEqual => Ok(Object::Boolean(left != right)),
            TokenType::Minus => (left - right)
                .map_err(|e| report_binary_expr_error(left_ty, operator, right_ty, Some(e))),
            TokenType::Plus => (left + right)
                .map_err(|e| report_binary_expr_error(left_ty, operator, right_ty, Some(e))),
            TokenType::Star => (left * right)
                .map_err(|e| report_binary_expr_error(left_ty, operator, right_ty, Some(e))),
            TokenType::Slash => {
                // handle division by zero attempts
                if right == Object::Number(0.0) {
                    return Err(RuntimeReturnValue::Error(RuntimeError::new(
                        operator,
                        "division by zero".to_string(),
                        "division by zero isn't possible".to_string(),
                    )));
                }
                (left / right)
                    .map_err(|e| report_binary_expr_error(left_ty, operator, right_ty, Some(e)))
            }
            _ => Err(report_binary_expr_error(
                left_ty,
                operator,
                right_ty,
                Some("only arithmetic operations are allowed in binary expressions"),
            )),
        }
    }

    fn visit_var_expr(&mut self, expr: &VarExpr) -> Result<Object, RuntimeReturnValue> {
        self.lookup_var(&expr.name)
    }

    fn visit_assign_expr(&mut self, expr: &AssignExpr) -> Result<Object, RuntimeReturnValue> {
        let new_val = self.evaluate(&expr.value)?;
        if let Some(distance) = self.locals.get(&expr.name) {
            self.environment
                .borrow_mut()
                .assign_at(*distance, &expr.name, &new_val)?;
        }
        self.globals.borrow_mut().assign(&expr.name, &new_val)?;
        Ok(new_val)
    }

    fn visit_logical_expr(&mut self, expr: &LogicalExpr) -> Result<Object, RuntimeReturnValue> {
        let left = self.evaluate(&expr.left)?;
        let left_truthy = is_truthy(left.clone());
        // left or right
        // if left is truthy, entire expr is truthy so return the truthy (i.e. left)
        // value
        if ((expr.operator.ty == TokenType::Or) && left_truthy)
        // left and right
        // if left is falsey, entire expr is falsey so return the falsey (i.e. left) value
        || ((expr.operator.ty == TokenType::And) && !left_truthy)
        {
            return Ok(left);
        }
        // otherwise right determines what the value is
        self.evaluate(&expr.right)
    }

    fn visit_call_expr(&mut self, expr: &CallExpr) -> Result<Object, RuntimeReturnValue> {
        let callee = self.evaluate(&expr.callee)?;
        let arguments: Result<Vec<_>, _> = expr
            .arguments
            .iter()
            .map(|argument| self.evaluate(argument))
            .collect();
        let arguments = arguments?;
        match callee {
            Object::Callable(ref function) => {
                let (expected, got, token, err_msg) = (
                    function.arity(),
                    arguments.len(),
                    &expr.closing_paren,
                    format!("invalid number of arguments for `{}`", function),
                );
                check_arity(expected, got, token, err_msg)?;
                function.call(self, &arguments)
            }
            Object::Class(ref class) => {
                let instance = Object::Instance(Rc::new(RefCell::new(LoxInstance::new(class))));
                if let Some(initializer) = class.borrow().find_method("init") {
                    let (expected, got, token, err_msg) = (
                        initializer.arity(),
                        arguments.len(),
                        &expr.closing_paren,
                        format!(
                            "invalid number of arguments for `{}` defined in `{}`",
                            initializer,
                            class.borrow()
                        ),
                    );
                    check_arity(expected, got, token, err_msg)?;
                    initializer.bind(instance.clone()).call(self, &arguments)?;
                }
                Ok(instance)
            }
            _ => Err(RuntimeReturnValue::Error(RuntimeError::new(
                &expr.closing_paren,
                format!("couldn't call object of type `{}`", callee),
                "only functions and classes can be called".to_string(),
            ))),
        }
    }

    fn visit_get_expr(&mut self, expr: &GetExpr) -> Result<Object, RuntimeReturnValue> {
        let object = self.evaluate(&expr.object)?;
        if let Object::Instance(ref instance) = object {
            return instance.borrow().get(&expr.name, &object);
        }
        Err(RuntimeReturnValue::Error(RuntimeError::new(
            &expr.name,
            format!("invalid operation on object of type `{}`", object.display()),
            "only class instances have properties".to_string(),
        )))
    }

    fn visit_set_expr(&mut self, expr: &SetExpr) -> Result<Object, RuntimeReturnValue> {
        let object = self.evaluate(&expr.object)?;
        if let Object::Instance(instance) = object {
            let value = self.evaluate(&expr.value)?;
            instance.borrow_mut().set(&expr.name, &value);
            return Ok(value);
        }
        Err(RuntimeReturnValue::Error(RuntimeError::new(
            &expr.name,
            format!("invalid operation on value of type `{}`", object.display()),
            "only instances have fields to get or set".to_string(),
        )))
    }

    fn visit_this_expr(&mut self, expr: &ThisExpr) -> Result<Object, RuntimeReturnValue> {
        self.lookup_var(&expr.keyword)
    }

    fn visit_super_expr(&mut self, expr: &SuperExpr) -> Result<Object, RuntimeReturnValue> {
        let distance = self
            .locals
            .get(&expr.keyword)
            .expect("should have a `super` keyword defined in the locals");
        let superclass = self
            .environment
            .borrow()
            .get_at(*distance, "super")
            .expect("should be able to get an environment bound to `super`");

        let instance = self
            .environment
            .borrow()
            .get_at(*distance - 1, "this")
            .expect(
                "offsetting the distance of `super` should give us `this` in the inner environment",
            );
        if let Object::Class(ref superclass) = superclass {
            if let Some(method) = superclass.borrow().find_method(&expr.method.lexeme) {
                return Ok(Object::Callable(method.bind(instance)));
            } else {
                // TODO: use RuntimeError(..).into() like this in other places too
                return Err(RuntimeError::new(
                    &expr.method,
                    format!("undefined property `{}`", expr.method),
                    format!(
                        "couldn't find property `{}` for `{}`",
                        expr.method,
                        superclass.borrow()
                    ),
                )
                .into());
            }
        } else {
            unreachable!("superclass should always be the variant `Object::Class`")
        }
    }
}

fn report_binary_expr_error(
    left: &str,
    operator: &Token,
    right: &str,
    help: Option<&str>,
) -> RuntimeReturnValue {
    RuntimeReturnValue::Error(RuntimeError::new(
        operator,
        format!(
            "cannot perform `{}` between values of type `{}` and `{}`",
            operator.lexeme, left, right
        ),
        help.unwrap_or("try parsing the operands as `number` or `str`")
            .to_string(),
    ))
}

fn check_arity(
    expected: usize,
    got: usize,
    token: &Token,
    err_msg: String,
) -> Result<(), RuntimeReturnValue> {
    if expected != got {
        Err(RuntimeReturnValue::Error(RuntimeError::new(
            token,
            err_msg,
            format!(
                "expected {} arguments, but {} arguments were supplied",
                expected, got
            ),
        )))
    } else {
        Ok(())
    }
}
