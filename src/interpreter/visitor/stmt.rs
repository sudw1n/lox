use std::{cell::RefCell, collections::HashMap, rc::Rc};

use crate::{
    error::RuntimeError,
    interpreter::{
        environment::Environment,
        object::{class::LoxClass, function::LoxFunction, Object},
        Interpreter, RuntimeReturnValue,
    },
    parser::grammar::{
        BlockStmt, ClassStmt, Expr, FunctionStmt, IfStmt, ReturnStmt, Stmt, StmtVisitor, VarStmt,
        WhileStmt,
    },
};

use super::is_truthy;

// statements produce no values so return ()
impl StmtVisitor<(), RuntimeReturnValue> for Interpreter {
    fn visit_print_stmt(&mut self, expr: &Expr) -> Result<(), RuntimeReturnValue> {
        let value = self.evaluate(expr)?;
        println!("{value}");
        Ok(())
    }

    fn visit_expression_stmt(&mut self, expr: &Expr) -> Result<(), RuntimeReturnValue> {
        let _ = self.evaluate(expr)?;
        Ok(())
    }

    fn visit_var_stmt(&mut self, stmt: &VarStmt) -> Result<(), RuntimeReturnValue> {
        let value = if let Some(initializer) = &stmt.initializer {
            self.evaluate(initializer)?
        } else {
            Object::Nil
        };
        self.environment
            .borrow_mut()
            .insert(stmt.name.lexeme.clone(), value);
        Ok(())
    }

    fn visit_block_stmt(&mut self, stmt: &BlockStmt) -> Result<(), RuntimeReturnValue> {
        self.execute_block(
            &stmt.statements,
            Rc::new(RefCell::new(Environment::from(&self.environment))),
        )
    }

    fn visit_if_stmt(&mut self, stmt: &IfStmt) -> Result<(), RuntimeReturnValue> {
        let condition_value = self.evaluate(&stmt.condition)?;
        if is_truthy(condition_value) {
            self.execute(&stmt.then_branch)?;
        } else if let Some(else_branch) = &stmt.else_branch {
            self.execute(else_branch)?;
        }
        Ok(())
    }

    fn visit_while_stmt(&mut self, stmt: &WhileStmt) -> Result<(), RuntimeReturnValue> {
        while is_truthy(self.evaluate(&stmt.condition)?) {
            self.execute(&stmt.body)?;
        }
        Ok(())
    }

    fn visit_function_stmt(&mut self, stmt: &FunctionStmt) -> Result<(), RuntimeReturnValue> {
        let function = (stmt, &self.environment, false).into();
        self.environment
            .borrow_mut()
            .insert(stmt.name.lexeme.clone(), Object::Callable(function));
        Ok(())
    }

    fn visit_return_stmt(&mut self, stmt: &ReturnStmt) -> Result<(), RuntimeReturnValue> {
        let return_value = self.evaluate(&stmt.value)?;
        Err(RuntimeReturnValue::Return(return_value))
    }

    fn visit_class_stmt(&mut self, stmt: &ClassStmt) -> Result<(), RuntimeReturnValue> {
        let superclass = stmt.superclass
            .as_ref()
            .map(|expr| {
                if let Object::Class(ref valid_class) = self.evaluate(expr)? {
                    Ok(Rc::clone(valid_class))
                }
                else if let Expr::Variable(ref invalid_class) = expr {
                    Err(RuntimeReturnValue::Error(RuntimeError::new(
                        &invalid_class.name,
                        format!(
                            "attempt at inheriting `{}` from an invalid class `{}`",
                            stmt.name.lexeme, invalid_class.name
                        ),
                        "a superclass must itself also be a class".to_string(),
                    )))
                }
                else {
                    unreachable!("superclass should always be an `Expr::Variable` variant and should evaluate to an `Object::Class`");
                }
        }).transpose()?;

        self.environment
            .borrow_mut()
            .insert(stmt.name.lexeme.clone(), Object::Nil);

        if let Some(superclass) = &superclass {
            self.environment = Rc::new(RefCell::new(Environment::from(&self.environment)));
            self.environment
                .borrow_mut()
                .insert("super".to_string(), Object::Class(Rc::clone(superclass)));
        }

        let methods: HashMap<String, LoxFunction> = stmt
            .methods
            .iter()
            .filter_map(|class_function| {
                if let Stmt::Function(method) = class_function {
                    Some((
                        method.name.lexeme.clone(),
                        (method, &self.environment, method.name.lexeme.eq("init")).into(),
                    ))
                } else {
                    None
                }
            })
            .collect();

        if superclass.is_some() {
            let parent = self
                .environment
                .borrow()
                .enclosing
                .clone()
                .expect("no enclosing environment for the superclass");
            self.environment = parent;
        }

        let class = Object::Class(Rc::new(RefCell::new(LoxClass::new(
            stmt.name.lexeme.clone(),
            superclass,
            methods,
        ))));

        self.environment.borrow_mut().assign(&stmt.name, &class)?;
        Ok(())
    }
}

impl From<(&FunctionStmt, &Rc<RefCell<Environment>>, bool)> for LoxFunction {
    fn from((stmt, env, is_initializer): (&FunctionStmt, &Rc<RefCell<Environment>>, bool)) -> Self {
        LoxFunction::UserDefined {
            name: stmt.name.clone(),
            params: stmt.params.clone(),
            body: stmt.body.clone(),
            closure: Rc::clone(env),
            is_initializer,
        }
    }
}
