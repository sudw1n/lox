use super::object::Object;

mod expr;
mod stmt;

fn is_truthy(object: Object) -> bool {
    !(!object) == Object::Boolean(true)
}
