pub mod environment;
pub mod native;
pub mod object;
pub mod visitor;
use native::define_clock;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use environment::Environment;
use object::Object;

use crate::error::RuntimeError;
use crate::parser::grammar::{Expr, Stmt};
use crate::scanner::token::Token;
use crate::LoxError;

/// This solely exists so that we can support return statements
#[derive(Debug, Clone)]
pub enum RuntimeReturnValue {
    Error(RuntimeError),
    Return(Object),
}

#[derive(Debug, Clone)]
pub struct Interpreter {
    /// Tracks the current scope
    environment: Rc<RefCell<Environment>>,
    /// Holds a fixed reference to the outermost global scope
    globals: Rc<RefCell<Environment>>,
    /// To store the resolution information
    locals: HashMap<Token, usize>,
}

impl Interpreter {
    pub fn new() -> Self {
        let globals = Rc::new(RefCell::new(Environment::new()));
        define_clock(&mut globals.borrow_mut());
        Self {
            environment: Rc::clone(&globals),
            locals: HashMap::new(),
            globals,
        }
    }

    /// Interpret the given expression and return the `Object` resulting from
    /// it.
    pub fn interpret(&mut self, program: &[Stmt]) -> Result<(), LoxError> {
        let mut errors = Vec::new();
        for statement in program {
            if let Err(RuntimeReturnValue::Error(e)) = self.execute(statement) {
                errors.push(e);
            }
        }
        if !errors.is_empty() {
            return Err(LoxError::RuntimeErrors(errors));
        }
        Ok(())
    }

    fn execute(&mut self, stmt: &Stmt) -> Result<(), RuntimeReturnValue> {
        stmt.accept(self)
    }

    fn evaluate(&mut self, expr: &Expr) -> Result<Object, RuntimeReturnValue> {
        expr.accept(self)
    }

    fn execute_block(
        &mut self,
        stmts: &[Stmt],
        environment: Rc<RefCell<Environment>>,
    ) -> Result<(), RuntimeReturnValue> {
        let previous = self.environment.clone();
        self.environment = environment;
        let result = stmts.iter().try_for_each(|stmt| self.execute(stmt));
        self.environment = previous;
        result
    }

    fn lookup_var(&self, name: &Token) -> Result<Object, RuntimeReturnValue> {
        if let Some(distance) = self.locals.get(name) {
            if let Some(var) = self.environment.borrow().get_at(*distance, &name.lexeme) {
                Ok(var)
            } else {
                Err(RuntimeReturnValue::Error(RuntimeError::new(
                    name,
                    "undefined variable".to_string(),
                    format!(
                        "couldn't find variable `{}` in the current scope",
                        &name.lexeme
                    ),
                )))
            }
        } else {
            self.globals.borrow().get(name)
        }
    }

    pub fn resolve(&mut self, name: &Token, depth: usize) {
        self.locals.insert(name.clone(), depth);
    }
}

impl Default for Interpreter {
    fn default() -> Self {
        Self::new()
    }
}

impl From<RuntimeError> for RuntimeReturnValue {
    fn from(value: RuntimeError) -> Self {
        RuntimeReturnValue::Error(value)
    }
}
