use super::Object;
use std::cmp::{Ordering, PartialOrd};
use std::ops::{Add, Div, Mul, Neg, Not, Sub};

impl Sub for Object {
    type Output = Result<Self, &'static str>;

    fn sub(self, rhs: Self) -> Self::Output {
        if let Object::Number(left) = self {
            if let Object::Number(right) = rhs {
                return Ok(Object::Number(left - right));
            }
        }
        Err("`-` only implemented for type `number`")
    }
}

impl Add for Object {
    type Output = Result<Self, &'static str>;

    fn add(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l + r)),
            (Object::Str(l), Object::Str(r)) => Ok(Object::Str(format!("{}{}", l, r))),
            (Object::Number(l), Object::Str(r)) => Ok(Object::Str(format!("{}{}", l, r))),
            (Object::Str(l), Object::Number(r)) => Ok(Object::Str(format!("{}{}", l, r))),
            (..) => Err("`+` only implemented for type `number` and `str`"),
        }
    }
}

impl Mul for Object {
    type Output = Result<Self, &'static str>;

    fn mul(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l * r)),
            (..) => Err("`*` only implemented for type `number` and `number`"),
        }
    }
}

impl Div for Object {
    type Output = Result<Self, &'static str>;

    fn div(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Object::Number(l), Object::Number(r)) => Ok(Object::Number(l / r)),
            (..) => Err("`/` only implemented for type `number` and `number`"),
        }
    }
}

impl Not for Object {
    type Output = Self;

    fn not(self) -> Self::Output {
        Object::Boolean(!is_truthy(&self))
    }
}

fn is_truthy(object: &Object) -> bool {
    match object {
        Object::Boolean(b) => *b,
        Object::Nil => false,
        _ => true,
    }
}

impl Neg for Object {
    type Output = Result<Object, &'static str>;

    fn neg(self) -> Self::Output {
        match self {
            Object::Number(n) => Ok(Object::Number(-n)),
            _ => Err("unary `-` only implemented for type `number`"),
        }
    }
}

impl PartialOrd for Object {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match (self, other) {
            // need to handle a special case in Lox where E: a == b is false if only a == nil, but
            // if b == null too then E is true
            (Object::Nil, o) => {
                if o == &Object::Nil {
                    Some(Ordering::Equal)
                } else {
                    None
                }
            }
            (Object::Number(left), Object::Number(right)) => left.partial_cmp(right),
            _ => None,
        }
    }
}
