use core::fmt;
use std::{cell::RefCell, fmt::Display, rc::Rc};

use crate::{
    interpreter::{environment::Environment, Interpreter, RuntimeReturnValue},
    parser::grammar::Stmt,
    scanner::token::Token,
};

use super::Object;

#[derive(Debug, Clone, PartialEq)]
pub enum LoxFunction {
    /// Anonymous implementation of a native function
    Native {
        arity: usize,
        body: Box<fn(&[Object]) -> Object>,
    },
    /// User-defined functions
    UserDefined {
        name: Token,
        params: Vec<Token>,
        body: Vec<Stmt>,
        closure: Rc<RefCell<Environment>>,
        is_initializer: bool,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FunctionType {
    None,
    Function,
    Initializer,
    Method,
}

impl LoxFunction {
    pub fn call(
        &self,
        interpreter: &mut Interpreter,
        arguments: &[Object],
    ) -> Result<Object, RuntimeReturnValue> {
        match self {
            Self::Native { body, .. } => Ok(body(arguments)),
            Self::UserDefined {
                params,
                body,
                closure,
                is_initializer,
                ..
            } => {
                let environment = Rc::new(RefCell::new(Environment::from(closure)));
                for (param, arg) in params.iter().zip(arguments.iter()) {
                    environment
                        .borrow_mut()
                        .insert(param.lexeme.clone(), arg.clone());
                }
                match interpreter.execute_block(body, environment) {
                    Err(RuntimeReturnValue::Return(value)) => {
                        let object = if *is_initializer {
                            closure
                                .borrow()
                                .get_at(0, "this")
                                .expect("initializer should return `this`")
                        } else {
                            value
                        };
                        Ok(object)
                    }
                    Err(other) => Err(other),
                    Ok(..) => {
                        let object = if *is_initializer {
                            closure
                                .borrow()
                                .get_at(0, "this")
                                .expect("initializer should return `this`")
                        } else {
                            Object::Nil
                        };
                        Ok(object)
                    }
                }
            }
        }
    }

    pub fn bind(&self, instance: Object) -> Self {
        match self {
            Self::UserDefined {
                name,
                params,
                body,
                closure,
                is_initializer,
            } => {
                let environment = Rc::new(RefCell::new(Environment::from(closure)));
                environment
                    .borrow_mut()
                    .insert("this".to_string(), instance);
                Self::UserDefined {
                    name: name.clone(),
                    params: params.clone(),
                    body: body.clone(),
                    closure: environment,
                    is_initializer: *is_initializer,
                }
            }
            Self::Native { .. } => unreachable!(),
        }
    }

    pub fn arity(&self) -> usize {
        match self {
            Self::Native { arity, .. } => *arity,
            Self::UserDefined { params, .. } => params.len(),
        }
    }
}

impl Display for LoxFunction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Native { body, .. } => write!(f, "<fun native @ {:p}>", body),
            Self::UserDefined { name, params, .. } => {
                let paramlist = params
                    .iter()
                    .map(|param| param.lexeme.as_str())
                    .collect::<Vec<&str>>()
                    .join(", ");
                write!(f, "<fun {}({})>", name.lexeme, paramlist)
            }
        }
    }
}

impl Display for FunctionType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Function => write!(f, "function"),
            Self::Method => write!(f, "method"),
            Self::Initializer => write!(f, "initializer"),
            Self::None => write!(f, "not-a-function"),
        }
    }
}

impl Default for FunctionType {
    fn default() -> Self {
        Self::None
    }
}
