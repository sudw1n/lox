pub mod class;
pub mod function;
pub mod ops;
use core::fmt;
use std::{cell::RefCell, rc::Rc};

use class::{LoxClass, LoxInstance};
use function::LoxFunction;

/// Represents the values used by Lox interpreter internally
#[derive(Debug, Clone, PartialEq)]
pub enum Object {
    Boolean(bool),
    Nil,
    Number(f64),
    Str(String),
    Callable(LoxFunction),
    Class(Rc<RefCell<LoxClass>>),
    Instance(Rc<RefCell<LoxInstance>>),
}

impl Object {
    /// Returns the type of the Object instance's variant.
    /// This is different from implementing `Display` as that is being used to
    /// print the actual value contained inside the Object instance.
    pub fn display(&self) -> &'static str {
        match self {
            Object::Boolean(_) => "boolean",
            Object::Nil => "nil",
            Object::Number(_) => "number",
            Object::Str(_) => "str",
            Object::Callable(_) => "callable",
            Object::Class(_) => "class",
            Object::Instance(_) => "instance",
        }
    }
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Object::Boolean(b) => write!(f, "{}", b),
            Object::Nil => write!(f, "nil"),
            Object::Number(n) => write!(f, "{}", n),
            Object::Str(s) => write!(f, "{}", s),
            Object::Callable(c) => write!(f, "{}", c),
            Object::Class(c) => write!(f, "{}", c.borrow()),
            Object::Instance(i) => write!(f, "{}", i.borrow()),
        }
    }
}
