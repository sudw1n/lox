use std::{cell::RefCell, collections::HashMap, fmt::Display, rc::Rc};

use crate::{error::RuntimeError, interpreter::RuntimeReturnValue, scanner::token::Token};

use super::{function::LoxFunction, Object};

#[derive(Debug, Clone, PartialEq)]
pub struct LoxClass {
    name: String,
    superclass: Option<Rc<RefCell<LoxClass>>>,
    methods: HashMap<String, LoxFunction>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ClassType {
    None,
    Class,
    SubClass,
}

impl LoxClass {
    pub fn new(
        name: String,
        superclass: Option<Rc<RefCell<LoxClass>>>,
        methods: HashMap<String, LoxFunction>,
    ) -> Self {
        Self {
            name,
            superclass,
            methods,
        }
    }

    pub fn find_method(&self, name: &str) -> Option<LoxFunction> {
        // Check if the method exists in the current class
        if let Some(method) = self.methods.get(name) {
            return Some(method.clone());
        }

        // Check if the method exists in the superclass (if any)
        if let Some(superclass) = &self.superclass {
            return superclass.borrow().find_method(name);
        }

        None
    }
}

impl Display for LoxClass {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<class {}>", self.name)
    }
}

impl Display for ClassType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::None => write!(f, "not-a-class"),
            Self::Class => write!(f, "class"),
            Self::SubClass => write!(f, "subclass"),
        }
    }
}

impl Default for ClassType {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct LoxInstance {
    class: Rc<RefCell<LoxClass>>,
    fields: HashMap<String, Object>,
}

impl LoxInstance {
    pub fn new(class: &Rc<RefCell<LoxClass>>) -> Self {
        Self {
            class: Rc::clone(class),
            fields: HashMap::new(),
        }
    }

    pub fn get(&self, name: &Token, instance: &Object) -> Result<Object, RuntimeReturnValue> {
        if let Some(field) = self.fields.get(&name.lexeme) {
            Ok(field.clone())
        } else if let Some(method) = self.class.borrow().find_method(&name.lexeme) {
            Ok(Object::Callable(method.bind(instance.clone())))
        } else {
            Err(RuntimeReturnValue::Error(RuntimeError::new(
                name,
                "invalid property name".to_string(),
                format!(
                    "the property `{}` isn't defined for value of type `{}`",
                    name.lexeme, self
                ),
            )))
        }
    }

    pub fn set(&mut self, name: &Token, value: &Object) {
        self.fields.insert(name.lexeme.clone(), value.clone());
    }
}

impl Display for LoxInstance {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{} instance>", self.class.borrow().name)
    }
}
