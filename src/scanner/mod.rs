pub mod token;

use std::collections::HashMap;

use crate::error::{LoxError, SyntaxError};
use token::{Token, TokenType};

#[derive(Debug)]
pub struct Scanner {
    source: String,
    start: usize,
    current: usize,
    line: usize,
    keywords: HashMap<&'static str, TokenType>,
    tokens: Vec<Token>,
}

impl Scanner {
    pub fn new(source: String) -> Self {
        let keywords = [
            ("and", TokenType::And),
            ("class", TokenType::Class),
            ("else", TokenType::Else),
            ("false", TokenType::False),
            ("for", TokenType::For),
            ("fun", TokenType::Fun),
            ("if", TokenType::If),
            ("nil", TokenType::Nil),
            ("or", TokenType::Or),
            ("print", TokenType::Print),
            ("return", TokenType::Return),
            ("super", TokenType::Super),
            ("this", TokenType::This),
            ("true", TokenType::True),
            ("var", TokenType::Var),
            ("while", TokenType::While),
        ]
        .iter()
        .cloned()
        .collect();

        Scanner {
            source,
            line: 1,
            start: 0,
            current: 0,
            keywords,
            tokens: Vec::new(),
        }
    }

    pub fn scan_tokens(&mut self) -> Result<Vec<Token>, LoxError> {
        let mut source_errors = Vec::new();
        while !self.is_at_end() {
            // we are at beginning of the next lexeme
            self.start = self.current;
            if let Err((err_msg, help_msg)) = self.scan_token() {
                source_errors.push(SyntaxError {
                    line: self.line,
                    span: (self.start, self.current).into(),
                    err_message: err_msg,
                    help_message: help_msg,
                });
            }
        }
        self.tokens.push(Token::new(
            TokenType::Eof,
            (self.start, self.current),
            "".to_string(),
            self.line,
        ));
        if !source_errors.is_empty() {
            return Err(LoxError::SyntaxErrors(source_errors));
        }
        Ok(self.tokens.clone())
    }

    fn scan_token(&mut self) -> Result<(), (String, String)> {
        let c = self.advance();
        match c {
            '(' => self.add_token(TokenType::LeftParen),
            ')' => self.add_token(TokenType::RightParen),
            '{' => self.add_token(TokenType::LeftBrace),
            '}' => self.add_token(TokenType::RightBrace),
            ',' => self.add_token(TokenType::Comma),
            '.' => self.add_token(TokenType::Dot),
            '-' => self.add_token(TokenType::Minus),
            '+' => self.add_token(TokenType::Plus),
            ';' => self.add_token(TokenType::Semicolon),
            '*' => self.add_token(TokenType::Star),
            '!' => {
                let ty = if self.check('=') {
                    TokenType::BangEqual
                } else {
                    TokenType::Bang
                };
                self.add_token(ty);
            }
            '=' => {
                let ty = if self.check('=') {
                    TokenType::EqualEqual
                } else {
                    TokenType::Equal
                };
                self.add_token(ty);
            }
            '<' => {
                let ty = if self.check('=') {
                    TokenType::LessEqual
                } else {
                    TokenType::Less
                };
                self.add_token(ty);
            }
            '>' => {
                let ty = if self.check('=') {
                    TokenType::GreaterEqual
                } else {
                    TokenType::Greater
                };
                self.add_token(ty);
            }
            '/' => {
                if self.check('/') {
                    // comment goes on until EOL
                    while self.peek() != '\n' && !self.is_at_end() {
                        self.advance();
                    }
                } else {
                    self.add_token(TokenType::Slash);
                }
            }
            ' ' | '\r' | '\t' => {
                // ignore whitespace
            }
            '\n' => {
                self.line += 1;
            }
            '"' => {
                self.handle_string()?;
            }
            i if i.is_alphabetic() || i == '_' => {
                self.handle_identifier();
            }
            d if d.is_ascii_digit() => {
                self.handle_number()?;
            }
            _ => {
                return Err((
                    "unexpected character".to_string(),
                    "try removing the character".to_string(),
                ));
            }
        }
        Ok(())
    }

    fn add_token(&mut self, ty: TokenType) {
        let lexeme = self.source[self.start..self.current].to_string();
        self.tokens.push(Token::new(
            ty,
            (self.start, self.current),
            lexeme,
            self.line,
        ))
    }

    fn handle_string(&mut self) -> Result<(), (String, String)> {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }
        if self.is_at_end() {
            return Err((
                "unterminated string".to_string(),
                "add a '\"' after the string".to_string(),
            ));
        }
        // consume the last '"'
        self.advance();

        // Trim the surrounding quotes
        let str_literal = self.source[self.start + 1..self.current - 1].to_string();
        self.add_token(TokenType::Str(str_literal));
        Ok(())
    }

    fn handle_number(&mut self) -> Result<(), (String, String)> {
        while self.peek().is_ascii_digit() {
            self.advance();
        }
        // Look for a fractional part.
        if self.peek() == '.' && self.peek_next().is_ascii_digit() {
            // consume the "."
            self.advance();
            while self.peek().is_ascii_digit() {
                self.advance();
            }
        }
        let number = &self.source[self.start..self.current];
        match number.parse() {
            Ok(num) => {
                self.add_token(TokenType::Number(num));
                Ok(())
            }
            Err(_) => Err((
                "couldn't parse number".to_string(),
                "make sure you're only using integers or floating point numbers".to_string(),
            )),
        }
    }

    fn handle_identifier(&mut self) {
        while self.peek().is_alphanumeric() || self.peek() == '_' {
            self.advance();
        }
        let identifier = &self.source[self.start..self.current];
        let ty = self
            .keywords
            .get(identifier)
            .cloned()
            .unwrap_or_else(|| TokenType::Identifier(identifier.to_string()));
        self.add_token(ty);
    }

    fn advance(&mut self) -> char {
        let current_char = self.char_at(self.current);
        self.current += 1;
        current_char
    }

    fn char_at(&self, idx: usize) -> char {
        self.source
            .chars()
            .nth(idx)
            .expect("given index should not be out-of-bounds for source")
    }

    /// We only consume the current character if it’s what we’re looking for.
    fn check(&mut self, expected: char) -> bool {
        if !self.is_at_end() && self.char_at(self.current) == expected {
            self.advance();
            return true;
        }
        false
    }

    fn peek(&self) -> char {
        self.source.chars().nth(self.current).unwrap_or('\0')
    }

    fn peek_next(&self) -> char {
        self.source.chars().nth(self.current + 1).unwrap_or('\0')
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.source.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn scan_tokens(source: &str) -> Result<Vec<Token>, LoxError> {
        Scanner::new(source.to_string()).scan_tokens()
    }

    #[test]
    fn scan_single_token() {
        let source = "+\n";
        let tokens = scan_tokens(source).unwrap();

        assert_eq!(tokens.len(), 2); // Including EOF token

        assert_eq!(tokens[0].ty, TokenType::Plus);
        assert_eq!(tokens[0].line, 1);

        assert_eq!(tokens[1].ty, TokenType::Eof);
        assert_eq!(tokens[1].lexeme, "");
        assert_eq!(tokens[1].line, 2);
    }

    #[test]
    fn scan_keyword() {
        let source = "fun\n";
        let tokens = scan_tokens(source).unwrap();

        assert_eq!(tokens.len(), 2);

        assert_eq!(tokens[0].ty, TokenType::Fun);
        assert_eq!(tokens[0].lexeme, "fun");
        assert_eq!(tokens[0].line, 1);

        assert_eq!(tokens[1].ty, TokenType::Eof);
        assert_eq!(tokens[1].lexeme, "");
        assert_eq!(tokens[1].line, 2);
    }

    #[test]
    fn scan_number_statement() {
        let source = "var x = 10.13;\n";
        let tokens = scan_tokens(source).unwrap();

        assert_eq!(tokens.len(), 6);

        assert_eq!(tokens[0].ty, TokenType::Var);
        assert_eq!(tokens[0].lexeme, "var");
        assert_eq!(tokens[0].line, 1);

        assert_eq!(tokens[1].ty, TokenType::Identifier("x".to_string()));
        assert_eq!(tokens[1].lexeme, "x");
        assert_eq!(tokens[1].line, 1);

        assert_eq!(tokens[2].ty, TokenType::Equal);
        assert_eq!(tokens[2].lexeme, "=");
        assert_eq!(tokens[2].line, 1);

        assert_eq!(tokens[3].ty, TokenType::Number(10.13));
        assert_eq!(tokens[3].lexeme, "10.13");
        assert_eq!(tokens[3].line, 1);

        assert_eq!(tokens[4].ty, TokenType::Semicolon);
        assert_eq!(tokens[4].lexeme, ";");
        assert_eq!(tokens[4].line, 1);

        assert_eq!(tokens[5].ty, TokenType::Eof);
        assert_eq!(tokens[5].lexeme, "");
        assert_eq!(tokens[5].line, 2);
    }

    #[test]
    fn scan_string_statement() {
        let expected_str = "Hello world";
        let source = format!("var x = \"{}\";\n", expected_str);
        let tokens = scan_tokens(&source).unwrap();

        assert_eq!(tokens.len(), 6);

        assert_eq!(tokens[0].ty, TokenType::Var);
        assert_eq!(tokens[0].lexeme, "var");
        assert_eq!(tokens[0].line, 1);

        assert_eq!(tokens[1].ty, TokenType::Identifier("x".to_string()));
        assert_eq!(tokens[1].lexeme, "x");
        assert_eq!(tokens[1].line, 1);

        assert_eq!(tokens[2].ty, TokenType::Equal);
        assert_eq!(tokens[2].lexeme, "=");
        assert_eq!(tokens[2].line, 1);

        assert_eq!(tokens[3].ty, TokenType::Str(expected_str.to_string()));
        let expected_lexeme = format!("\"{}\"", expected_str);
        assert_eq!(tokens[3].lexeme, expected_lexeme);
        assert_eq!(tokens[3].line, 1);

        assert_eq!(tokens[4].ty, TokenType::Semicolon);
        assert_eq!(tokens[4].lexeme, ";");
        assert_eq!(tokens[4].line, 1);

        assert_eq!(tokens[5].ty, TokenType::Eof);
        assert_eq!(tokens[5].lexeme, "");
        assert_eq!(tokens[5].line, 2);
    }
}
