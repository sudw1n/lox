use std::{
    fmt::{Debug, Display, Formatter, Result},
    hash::{Hash, Hasher},
};

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
    pub ty: TokenType,
    pub lexeme: String,
    pub line: usize,
    pub span: (usize, usize),
}

impl Token {
    pub fn new(ty: TokenType, span: (usize, usize), lexeme: String, line: usize) -> Self {
        Token {
            ty,
            lexeme,
            span,
            line,
        }
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "[line {} @ {}:{}] {}: {}",
            self.line, self.span.0, self.span.1, self.lexeme, self.ty
        )
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    // One or two character tokens.
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals.
    Identifier(String),
    Str(String),
    Number(f64),

    // Keywords.
    And,
    Class,
    Else,
    False,
    Fun,
    For,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    Eof,
}

impl Display for TokenType {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let token_str = match self {
            TokenType::LeftParen => "LeftParen",
            TokenType::RightParen => "RightParen",
            TokenType::LeftBrace => "LeftBrace",
            TokenType::RightBrace => "RightBrace",
            TokenType::Comma => "Comma",
            TokenType::Dot => "Dot",
            TokenType::Minus => "Minus",
            TokenType::Plus => "Plus",
            TokenType::Semicolon => "Semicolon",
            TokenType::Slash => "Slash",
            TokenType::Star => "Star",
            TokenType::Bang => "Bang",
            TokenType::BangEqual => "BangEqual",
            TokenType::Equal => "Equal",
            TokenType::EqualEqual => "EqualEqual",
            TokenType::Greater => "Greater",
            TokenType::GreaterEqual => "GreaterEqual",
            TokenType::Less => "Less",
            TokenType::LessEqual => "LessEqual",
            TokenType::And => "And",
            TokenType::Class => "Class",
            TokenType::Else => "Else",
            TokenType::False => "False",
            TokenType::Fun => "Fun",
            TokenType::For => "For",
            TokenType::If => "If",
            TokenType::Nil => "Nil",
            TokenType::Or => "Or",
            TokenType::Print => "Print",
            TokenType::Return => "Return",
            TokenType::Super => "Super",
            TokenType::This => "This",
            TokenType::True => "True",
            TokenType::Var => "Var",
            TokenType::While => "While",
            TokenType::Eof => "Eof",
            TokenType::Identifier(_) => "Identifier",
            TokenType::Str(_) => "Str",
            TokenType::Number(_) => "Number",
        };
        write!(
            f,
            "{}{}",
            token_str,
            match self {
                TokenType::Identifier(id) => format!(" {id}"),
                TokenType::Str(s) => format!(" {s}"),
                TokenType::Number(n) => format!(" {n}"),
                _ => "".to_string(),
            }
        )
    }
}

impl Hash for Token {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.lexeme.hash(state);
        self.line.hash(state);
    }
}

impl Eq for Token {}

#[cfg(test)]
mod tests {
    use super::*;

    // ensure the display format is consistent
    #[test]
    fn token_display_format() {
        let token = Token::new(TokenType::Number(10.11), (0, 4), "10.11".to_string(), 9);
        assert_eq!(format!("{}", token), "[line 9 @ 0:4] 10.11: Number 10.11");
    }
}
